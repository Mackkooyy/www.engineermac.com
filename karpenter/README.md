### Git push

git push --set-upstream origin <branch> -o merge_request.target=main -o merge_request.create -o merge_request.merge_when_pipeline_succeeds -o merge_request.remove_source_branch

### Deployment Notes
https://karpenter.sh/v0.20.0/concepts/provisioning/
https://karpenter.sh/v0.20.0/getting-started/getting-started-with-terraform/

You need to update your AWS CLI to >2.7.25 or the latest (recommended),
Ensure your CLI is pointing to the right region.

# On nginx-ingress-controller
# Source: https://kubernetes.github.io/ingress-nginx/deploy/#aws

Edit the file and change the VPC CIDR in use for the Kubernetes cluster:
proxy-real-ip-cidr: XXX.XXX.XXX/XX

Change the AWS Certificate Manager (ACM) ID as well:
arn:aws:acm:us-west-2:XXXXXXXX:certificate/XXXXXX-XXXXXXX-XXXXXXX-XXXXXXXX