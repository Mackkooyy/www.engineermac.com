engineermac.com is a personal static-content oriented website created using create-react-app.
It has links directing to my LinkedIn profile and GitLab Repository.

It was hosted on AWS EKS with SSL certificate managed by AWS Certificate Manager and used NGINX as the
Ingress Controller, but currently hosted on AWS S3.

Karpenter is used for capacity management that directly provisions/terminates Nodes and underlying instances
based on Pod requirements in a matter of seconds.

Version control using GitLab with CI/CD pipeline of building the docker image using Kaniko and pushing it to Docker Hub.
It uses Node as the base image and NGINX as the final image to be lightweight.

### Git push

git push --set-upstream origin <branch> merge_request.target=main merge_request.create -o merge_request.merge_when_pipeline_succeeds -o merge_request.remove_source_branch

### Deployment Notes
https://karpenter.sh/v0.20.0/concepts/provisioning/
https://karpenter.sh/v0.20.0/getting-started/getting-started-with-terraform/

You need to update your AWS CLI to >2.7.25 or the latest (recommended),
Ensure your CLI is pointing to the right region.


# On nginx-ingress-controller
# Source: https://kubernetes.github.io/ingress-nginx/deploy/#aws
Edit the file and change the VPC CIDR in use for the Kubernetes cluster:

proxy-real-ip-cidr: XXX.XXX.XXX/XX

Change the AWS Certificate Manager (ACM) ID as well:
arn:aws:acm:us-west-2:XXXXXXXX:certificate/XXXXXX-XXXXXXX-XXXXXXX-XXXXXXXX
