import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';

test('engineermac.com test', () => {
  console.log('Unit Test');
  render(<Router><App /></Router>);
  const linkElement = screen.getByText(/AWS DevOps Engineer/i);
  expect(linkElement).toBeInTheDocument();
});

