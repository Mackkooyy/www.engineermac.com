terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region  = "ap-southeast-1"

}

module "kubernetes" {
  source                  = "./kubernetes"
  vpc_cidr_block          = "192.168.0.0/16"        #"10.240.0.0/16"
  ami                     = "ami-0434d3922e4794012" # My own k8s_cluster AMI
  cluster_name            = "engineermac"
  master_instance_type    = "t3.medium"
  nodes_max_size          = 1
  nodes_min_size          = 1
  private_subnet01_netnum = "1"
  public_subnet01_netnum  = "2"
  region                  = "ap-southeast-1"
  worker_instance_type    = "t3.medium"
  vpc_name                = "kubernetes"
}
